package org.judison.mutils;

import org.bson.types.ObjectId;

import com.mongodb.client.MongoClient;

public class DB extends MDB {

	public final MCollection<ObjectId, Pessoa> pessoas = getCollection("pessoas", ObjectId.class, Pessoa.class, p -> p.id);

	public DB(MongoClient client, String name) {
		super(client, name);
	}

}
