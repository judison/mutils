package org.judison.mutils;

import org.bson.types.ObjectId;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;

public class Teste {

	public static void main(String[] args) {

		try (MongoClient client = MongoClients.create()) {
			DB db = new DB(client, "teste");

//			Pessoa judison = new Pessoa();
//			judison.nome = "Alessandro";
//			judison.idade = 39;
//			db.pessoas.insertOne(judison);

			Filter query = new Filter();
			query.equal("idade", 40);
			query.greater("idade", 18);

			for (Pessoa pessoa: db.pessoas.find(query).sort("idade")) {
				System.out.println(pessoa.id);
				System.out.println(pessoa.nome);
				System.out.println(pessoa.idade);
			}

			db.pessoas.loadAsDoc(new ObjectId("62e193e67b20794c73c4abf7"));

			for (Doc document: db.pessoas.findAsDoc().projection("nome")) {
				System.out.println(document);
			}
		}
	}

}
