package org.judison.mutils;

import org.bson.BsonDocument;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.conversions.Bson;

class BaseBson implements Bson {

	protected final Doc doc = new Doc();

	@Override
	public <TDocument> BsonDocument toBsonDocument(Class<TDocument> tDocumentClass, CodecRegistry codecRegistry) {
		return doc.toBsonDocument(tDocumentClass, codecRegistry);
	}

	@Override
	public BsonDocument toBsonDocument() {
		return toBsonDocument(BsonDocument.class, MRegistry.CODEC_REGISTRY);
	}

	public Doc toDoc() {
		return new Doc(doc);
	}

	@Override
	public String toString() {
		return doc.toString();
	}

}
