package org.judison.mutils;

import com.mongodb.client.FindIterable;

public class FindItr<T> extends BaseFindItr<T, FindItr<T>> {

	FindItr(FindIterable<T> itr) {
		super(itr);
	}

	@Override
	protected FindItr<T> _update(FindIterable<T> itr) {
		if (this.itr == itr)
			return this;
		else
			return new FindItr<>(itr);
	}

}


