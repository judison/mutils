package org.judison.mutils;

import java.util.function.Function;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.ReplaceOptions;

public class MCollection<K, V> {

	private final MDB db;
	private final String name;
	private final Class<K> keyClass;
	private final Class<V> valueClass;
	private final Function<V, K> getId;
	private MongoCollection<V> coll;

	public MCollection(MDB db, String name, Class<K> keyClass, Class<V> valueClass, Function<V, K> getId) {
		this.db = db;
		this.name = name;
		this.keyClass = keyClass;
		this.valueClass = valueClass;
		this.getId = getId;
	}

	private void check() {
		if (coll == null)
			coll = db.getMongoDatabase().getCollection(name, valueClass);
	}

	public V load(K id) {
		check();
		return coll.find(new Filter().equal("_id", id)).first();
	}

	public Doc loadAsDoc(K id) {
		check();
		Object f = coll.find(new Filter().equal("_id", id), Doc.class).first();
		System.out.println("Class: " + f.getClass().getName());
		return (Doc)f;
	}

	public void save(V obj) {
		check();
		K id = getId.apply(obj);
		if (id == null)
			throw new IllegalArgumentException("Missing id");
		coll.replaceOne(new Filter().equal("_id", id), obj, new ReplaceOptions().upsert(true));
	}

	public void insertOne(V obj) {
		check();
		coll.insertOne(obj);
	}

	public boolean replaceOne(Filter filter, V obj) {
		check();
		return coll.replaceOne(filter, obj).getModifiedCount() > 0;
	}

	public void delete(K id) {
		check();
		coll.deleteOne(new Filter().equal("_id", id));
	}

	public void deleteOne(Filter filter) {
		check();
		coll.deleteOne(filter);
	}

	public void deleteMany(Filter filter) {
		check();
		coll.deleteMany(filter);
	}

	public FindItr<V> find() {
		check();
		return new FindItr<V>(coll.find());
	}

	public FindItr<V> find(Filter filter) {
		check();
		return new FindItr<V>(coll.find(filter));
	}

	public FindDocItr findAsDoc() {
		check();
		return new FindDocItr(coll.find(Doc.class));
	}

	public FindDocItr findAsDoc(Filter filter) {
		check();
		return new FindDocItr(coll.find(filter, Doc.class));
	}

	public boolean updateOne(Filter filter, Update update) {
		check();
		return coll.updateOne(filter, update).getModifiedCount() > 0;
	}

	public long updateMany(Filter filter, Update update) {
		check();
		return coll.updateMany(filter, update).getModifiedCount();
	}

	public long count() {
		check();
		return coll.countDocuments();
	}

	public long count(Filter filter) {
		check();
		return coll.countDocuments(filter);
	}

	public <T> DistinctItr<T> distinct(String field, Class<T> cls) {
		check();
		return new DistinctItr<>(coll.distinct(field, cls));
	}

	public <T> DistinctItr<T> distinct(String field, Filter filter, Class<T> cls) {
		check();
		return new DistinctItr<>(coll.distinct(field, filter, cls));
	}

	public String createIndex(Sort sort, IndexOptions indexOptions) {
		check();
		return coll.createIndex(sort, indexOptions);
	}

	public String createIndex(Sort sort) {
		check();
		return coll.createIndex(sort);
	}

	public void dropIndex(Sort sort) {
		check();
		coll.dropIndex(sort);
	}

	public void dropIndex(String name) {
		check();
		coll.dropIndex(name);
	}

	public AggregateItr<Doc> aggregate(Pipeline pipeline) {
		return aggregate(pipeline, Doc.class);
	}

	public <T> AggregateItr<T> aggregate(Pipeline pipeline, Class<T> cls) {
		check();
		return new AggregateItr<T>(coll.aggregate(pipeline.toDocList(), cls));
	}

}
