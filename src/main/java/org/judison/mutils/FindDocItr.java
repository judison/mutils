package org.judison.mutils;

import com.mongodb.client.FindIterable;
import com.mongodb.lang.Nullable;

public class FindDocItr extends BaseFindItr<Doc, FindDocItr> {

	FindDocItr(FindIterable<Doc> itr) {
		super(itr);
	}

	@Override
	protected FindDocItr _update(FindIterable<Doc> itr) {
		if (this.itr == itr)
			return this;
		else
			return new FindDocItr(itr);
	}

	/**
	 * Sets a document describing the fields to return for all matching documents.
	 *
	 * @param projection the project document, which may be null.
	 */
	public FindDocItr projection(@Nullable Projection projection) {
		return _update(itr.projection(projection));
	}

	/**
	 * Sets a document describing the fields to return for all matching documents.
	 */
	public FindDocItr projection(String... fields) {
		return _update(itr.projection(new Projection(fields)));
	}

}


