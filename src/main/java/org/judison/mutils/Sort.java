package org.judison.mutils;

public class Sort extends BaseBson {

	public Sort() {}

	public Sort(String... fields) {
		add(fields);
	}

	public Sort asc(String field) {
		doc.put(field, +1);
		return this;
	}

	public Sort desc(String field) {
		doc.put(field, -1);
		return this;
	}

	public Sort add(String... fields) {
		for (String field: fields)
			if (field.charAt(0) == '-')
				doc.put(field.substring(1), -1);
			else if (field.charAt(0) == '+')
				doc.put(field.substring(1), +1);
			else
				doc.put(field, +1);
		return this;
	}

}