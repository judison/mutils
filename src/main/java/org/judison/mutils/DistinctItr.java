package org.judison.mutils;

import java.util.Collection;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import com.mongodb.client.DistinctIterable;
import com.mongodb.client.model.Collation;
import com.mongodb.lang.Nullable;

public class DistinctItr<T> implements Iterable<T> {

	protected final DistinctIterable<T> itr;

	DistinctItr(DistinctIterable<T> itr) {
		this.itr = itr;
	}

	private DistinctItr<T> _update(DistinctIterable<T> itr) {
		if (itr != this.itr)
			return new DistinctItr<>(itr);
		else
			return this;
	}

	/**
	 * Iterates over all the documents, adding each to the given target.
	 *
	 * @param target the collection to insert into
	 * @param <A>    the collection type
	 * @return the target
	 */
	public <A extends Collection<? super T>> A into(A target) {
		return itr.into(target);
	}

	/**
	 * Sets the query filter to apply to the query.
	 *
	 * @param filter the filter, which may be null.
	 */
	public DistinctItr<T> filter(@Nullable Filter filter) {
		return _update(itr.filter(filter));
	}

	/**
	 * Sets the maximum execution time on the server for this operation.
	 *
	 * @param maxTime  the max time
	 * @param timeUnit the time unit, which may not be null
	 */
	public DistinctItr<T> maxTime(long maxTime, TimeUnit timeUnit) {
		return _update(itr.maxTime(maxTime, timeUnit));
	}

	/**
	 * Sets the number of documents to return per batch.
	 *
	 * @param batchSize the batch size
	 */
	public DistinctItr<T> batchSize(int batchSize) {
		return _update(itr.batchSize(batchSize));
	}

	/**
	 * Sets the collation options
	 *
	 * <p>A null value represents the server default.</p>
	 *
	 * @param collation the collation options to use
	 */
	public DistinctItr<T> collation(@Nullable Collation collation) {
		return _update(itr.collation(collation));
	}

	/**
	 * Sets the comment for this operation. A null value means no comment is set.
	 *
	 * @param comment the comment
	 */
	public DistinctItr<T> comment(@Nullable String comment) {
		return _update(itr.comment(comment));
	}

	@Override
	public Iterator<T> iterator() {
		return itr.iterator();
	}

	@Override
	public Spliterator<T> spliterator() {
		return itr.spliterator();
	}

	@Override
	public void forEach(Consumer<? super T> action) {
		itr.forEach(action);
	}

}
