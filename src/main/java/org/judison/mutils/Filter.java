package org.judison.mutils;

import java.util.ArrayList;
import java.util.List;

import org.bson.BsonRegularExpression;

@SuppressWarnings({"ManualArrayToCollectionCopy", "UseBulkOperation"})
public class Filter extends BaseBson {

	public enum Operator {
		// Comparison
		EQUAL("$eq"),
		GREATER_THAN("$gt"),
		GREATER_THAN_OR_EQUAL("$gte"),
		IN("$in"),
		LESS_THAN("$lt"),
		LESS_THAN_OR_EQUAL("$lte"),
		NOT_EQUAL("$ne"),
		NOT_IN("$nin"),
		// Logical
		// $and
		NOT("$not"),
		// $nor
		// $or
		// Element
		EXISTS("$exists"),
		TYPE("$type"),
		// Evaluation
		// $expr
		// $jsonSchema
		MOD("$mod"),
		REGEX("$regex"),
		TEXT("$text"),
		WHERE("$where"),
		// Geospatial
		// $geoIntersects
		// $geoWithin
		NEAR("$near"),
		NEAR_SPHERE("$nearSphere"),
		// Array
		ALL("$all"),
		ELEMENT_MATCH("$elemMatch"),
		SIZE("$size"),
		// Bitwise
		// ???
		WITHIN("$within"),
		WITHIN_CIRCLE("$center"),
		WITHIN_CIRCLE_SPHERE("$centerSphere"),
		WITHIN_BOX("$box"),
		;

		private final String value;

		Operator(String val) {
			value = val;
		}

		private boolean equals(String val) {
			return value.equals(val);
		}

		public static Operator fromString(String val) {
			for (int i = 0; i < values().length; i++) {
				Operator fo = values()[i];
				if (fo.equals(val))
					return fo;
			}
			return null;
		}
	}

	public Filter() {}

	protected Operator fop(String operator) {
		operator = operator.trim();

		if (operator.equals("=") || operator.equals("=="))
			return Operator.EQUAL;
		else if (operator.equals(">"))
			return Operator.GREATER_THAN;
		else if (operator.equals(">="))
			return Operator.GREATER_THAN_OR_EQUAL;
		else if (operator.equals("<"))
			return Operator.LESS_THAN;
		else if (operator.equals("<="))
			return Operator.LESS_THAN_OR_EQUAL;
		else if (operator.equals("!=") || operator.equals("<>"))
			return Operator.NOT_EQUAL;
		else if (operator.equalsIgnoreCase("in"))
			return Operator.IN;
		else if (operator.equalsIgnoreCase("nin"))
			return Operator.NOT_IN;
		else if (operator.equalsIgnoreCase("all"))
			return Operator.ALL;
		else if (operator.equalsIgnoreCase("exists"))
			return Operator.EXISTS;
		else if (operator.equalsIgnoreCase("elem"))
			return Operator.ELEMENT_MATCH;
		else if (operator.equalsIgnoreCase("size"))
			return Operator.SIZE;
		else if (operator.equalsIgnoreCase("within"))
			return Operator.WITHIN;
		else if (operator.equalsIgnoreCase("near"))
			return Operator.NEAR;
		else
			throw new IllegalArgumentException("Unknown operator '" + operator + "'");
	}

	public Filter filter(String condition, Object value) {
		String[] parts = condition.trim().split(" ");
		if (parts.length < 1 || parts.length > 2)
			throw new IllegalArgumentException("'" + condition + "' is not a legal filter condition");

		String prop = parts[0].trim();
		Operator oper = (parts.length == 2) ? fop(parts[1]) : Operator.EQUAL;

		return filter(prop, oper, value);
	}

	public Filter filter(String prop, Operator oper, Object value) {
		if (oper == Operator.EQUAL)
			doc.put(prop, value);
		else {
			Object inner = doc.get(prop); // operator within inner object
			if (!(inner instanceof Doc)) {
				inner = new Doc();
				doc.put(prop, inner);
			}
			((Doc)inner).put(oper.value, value);
		}
		return this;
	}

	public Filter equal(String prop, Object value) {
		return filter(prop, Operator.EQUAL, value);
	}

	public Filter notEqual(String prop, Object value) {
		return filter(prop, Operator.NOT_EQUAL, value);
	}

	public Filter greater(String prop, Object value) {
		return filter(prop, Operator.GREATER_THAN, value);
	}

	public Filter greaterOrEqual(String prop, Object value) {
		return filter(prop, Operator.GREATER_THAN_OR_EQUAL, value);
	}

	public Filter less(String prop, Object value) {
		return filter(prop, Operator.LESS_THAN, value);
	}

	public Filter lessOrEqual(String prop, Object value) {
		return filter(prop, Operator.LESS_THAN_OR_EQUAL, value);
	}

	public Filter exists(String prop) {
		return filter(prop, Operator.EXISTS, Boolean.TRUE);
	}

	public Filter notExists(String prop) {
		return filter(prop, Operator.EXISTS, Boolean.FALSE);
	}

	public Filter in(String prop, Object... values) {
		List<Object> list = new ArrayList<>();
		for (Object value: values)
			list.add(value);
		return filter(prop, Operator.IN, list);
	}

	public Filter notIn(String prop, Object... values) {
		List<Object> list = new ArrayList<>();
		for (Object value: values)
			list.add(value);
		return filter(prop, Operator.NOT_IN, list);
	}

	public Filter or(Filter... queries) {
		if (queries == null || queries.length < 2)
			throw new IllegalArgumentException("Must have at least 2 sub queries");
		List<Object> group = new ArrayList<>();
		for (Filter query: queries)
			group.add(query);
		filter("$or", Operator.EQUAL, group);
		return this;
	}

	public Filter and(Filter... queries) {
		if (queries == null || queries.length < 2)
			throw new IllegalArgumentException("Must have at least 2 sub queries");
		List<Object> group = new ArrayList<>();
		for (Filter query: queries)
			group.add(query);
		filter("$and", Operator.EQUAL, group);
		return this;
	}

	public Filter text(String query) {
		Doc obj = new Doc();
		obj.put("$search", query);
		filter("$text", Operator.EQUAL, obj);
		return this;
	}

	public Filter text(String query, String lang) {
		Doc obj = new Doc();
		obj.put("$search", query);
		obj.put("$language", lang);
		filter("$text", Operator.EQUAL, obj);
		return this;
	}

	public Filter regex(String prop, String regexPattern) {
		filter(prop, Operator.REGEX, new BsonRegularExpression(regexPattern));
		return this;
	}

	public Filter regex(String prop, String regexPattern, String options) {
		filter(prop, Operator.REGEX, new BsonRegularExpression(regexPattern, options));
		return this;
	}

}