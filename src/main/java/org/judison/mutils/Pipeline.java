package org.judison.mutils;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
public class Pipeline {

	private final List<Doc> stages = new ArrayList<>();

	Doc[] toDocArray() {
		return stages.toArray(new Doc[0]);
	}

	List<Doc> toDocList() {
		return new ArrayList<>(stages);
	}

	public Pipeline group(Group group) {
		stages.add(new Doc("$group", group.toDoc()));
		return this;
	}

	public Pipeline limit(int limit) {
		stages.add(new Doc("$limit", limit));
		return this;
	}

	public Pipeline match(Filter filter) {
		stages.add(new Doc("$match", filter.toDoc()));
		return this;
	}

	public Pipeline project(Projection projection) {
		stages.add(new Doc("$project", projection.toDoc()));
		return this;
	}

	public Pipeline set(Doc fields) {
		stages.add(new Doc("$set", fields));
		return this;
	}

	public Pipeline skip(int skip) {
		stages.add(new Doc("$skip", skip));
		return this;
	}

	public Pipeline sort(String... fields) {
		stages.add(new Doc("$sort", new Sort(fields).toDoc()));
		return this;
	}

	public Pipeline sort(Sort sort) {
		stages.add(new Doc("$sort", sort.toDoc()));
		return this;
	}

	public Pipeline unionWith(String coll, Pipeline pipeline) {
		Doc conf = new Doc("coll", coll);
		if (pipeline != null)
			conf.put("pipeline", pipeline.toDocArray());
		stages.add(new Doc("$unionWith", conf));
		return this;
	}

	public Pipeline unset(String... fields) {
		if (fields.length == 0)
			return this;
		stages.add(new Doc("$unset", fields));
		return this;
	}

	public Pipeline unwind(String path) {
		return unwind(path, null, false);
	}

	public Pipeline unwind(String path, String includeArrayIndex, boolean preserveNullAndEmptyArrays) {
		Doc conf = new Doc("path", path);
		if (includeArrayIndex != null)
			conf.put("includeArrayIndex", includeArrayIndex);
		if (preserveNullAndEmptyArrays)
			conf.put("preserveNullAndEmptyArrays", true);
		stages.add(new Doc("$unwind", conf));
		return this;
	}

}
