package org.judison.mutils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import org.bson.Document;

import com.mongodb.ExplainVerbosity;
import com.mongodb.client.FindIterable;
import com.mongodb.client.model.Collation;
import com.mongodb.lang.Nullable;

abstract class BaseFindItr<T, I extends BaseFindItr<T, I>> implements Iterable<T> {

	protected final FindIterable<T> itr;

	BaseFindItr(FindIterable<T> itr) {
		this.itr = itr;
	}

	protected abstract I _update(FindIterable<T> itr);

	/**
	 * Helper to return the first item in the iterator or null.
	 *
	 * @return T the first item or null.
	 */
	@Nullable
	public T first() {
		return itr.first();
	}

	/**
	 * Iterates over all the documents, adding each to the given target.
	 *
	 * @param target the collection to insert into
	 * @param <A>    the collection type
	 * @return the target
	 */
	public <A extends Collection<? super T>> A into(A target) {
		return itr.into(target);
	}

	public List<T> toList() {
		return itr.into(new ArrayList<>());
	}

	/**
	 * Sets the query filter to apply to the query.
	 *
	 * @param filter the filter, which may be null.
	 */
	public I filter(@Nullable Filter filter) {
		return _update(itr.filter(filter));
	}

	/**
	 * Sets the limit to apply.
	 *
	 * @param limit the limit, which may be 0
	 */
	public I limit(int limit) {
		return _update(itr.limit(limit));
	}

	/**
	 * Sets the number of documents to skip.
	 *
	 * @param skip the number of documents to skip
	 */
	public I skip(int skip) {
		return _update(itr.skip(skip));
	}

	public I skipLimit(int skip, int limit) {
		return _update(itr.skip(skip).limit(limit));
	}

	/**
	 * Sets the maximum execution time on the server for this operation.
	 *
	 * @param maxTime  the max time
	 * @param timeUnit the time unit, which may not be null
	 */
	public I maxTime(long maxTime, TimeUnit timeUnit) {
		return _update(itr.maxTime(maxTime, timeUnit));
	}

	//FindItr<T> maxAwaitTime(long maxAwaitTime, TimeUnit timeUnit);

	/**
	 * Sets the sort criteria to apply to the query.
	 *
	 * @param sort the sort criteria, which may be null.
	 */
	public I sort(@Nullable Sort sort) {
		return _update(itr.sort(sort));
	}

	/**
	 * Sets the sort criteria to apply to the query.
	 */
	public I sort(String... fields) {
		return _update(itr.sort(new Sort(fields)));
	}

	/**
	 * The server normally times out idle cursors after an inactivity period (10 minutes)
	 * to prevent excess memory use. Set this option to prevent that.
	 *
	 * @param noCursorTimeout true if cursor timeout is disabled
	 */
	public I noCursorTimeout(boolean noCursorTimeout) {
		return _update(itr.noCursorTimeout(noCursorTimeout));
	}

	//FindItr<T> partial(boolean partial);
	//FindItr<T> cursorType(CursorType cursorType);

	/**
	 * Sets the number of documents to return per batch.
	 *
	 * @param batchSize the batch size
	 */
	public I batchSize(int batchSize) {
		return _update(itr.batchSize(batchSize));
	}

	/**
	 * Sets the collation options
	 *
	 * <p>A null value represents the server default.</p>
	 *
	 * @param collation the collation options to use
	 */
	public I collation(@Nullable Collation collation) {
		return _update(itr.collation(collation));
	}

	/**
	 * Sets the comment for this operation. A null value means no comment is set.
	 *
	 * @param comment the comment
	 */
	public I comment(@Nullable String comment) {
		return _update(itr.comment(comment));
	}

	//FindItr<T> comment(@Nullable BsonValue comment);
	//FindItr<T> hint(@Nullable Bson hint);
	//FindItr<T> hintString(@Nullable String hint);

	//FindItr<T> let(@Nullable Bson variables);

	//FindItr<T> max(@Nullable Bson max);
	//FindItr<T> min(@Nullable Bson min);

	//FindItr<T> returnKey(boolean returnKey);

	/**
	 * Sets the showRecordId. Set to true to add a field {@code $recordId} to the returned documents.
	 *
	 * @param showRecordId the showRecordId
	 */
	public I showRecordId(boolean showRecordId) {
		return _update(itr.showRecordId(showRecordId));
	}

	//FindItr<T> allowDiskUse(@Nullable Boolean allowDiskUse);

	/**
	 * Explain the execution plan for this operation with the server's default verbosity level
	 *
	 * @return the execution plan
	 */
	public Document explain() {
		return itr.explain();
	}

	/**
	 * Explain the execution plan for this operation with the given verbosity level
	 *
	 * @param verbosity the verbosity of the explanation
	 * @return the execution plan
	 */
	public Document explain(ExplainVerbosity verbosity) {
		return itr.explain(verbosity);
	}

	@Override
	public Iterator<T> iterator() {
		return itr.iterator();
	}

	@Override
	public Spliterator<T> spliterator() {
		return itr.spliterator();
	}

	@Override
	public void forEach(Consumer<? super T> action) {
		itr.forEach(action);
	}

}
