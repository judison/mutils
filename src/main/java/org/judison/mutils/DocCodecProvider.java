package org.judison.mutils;

import java.util.Objects;

import org.bson.Transformer;
import org.bson.codecs.BsonTypeClassMap;
import org.bson.codecs.Codec;
import org.bson.codecs.configuration.CodecProvider;
import org.bson.codecs.configuration.CodecRegistry;

import static org.bson.assertions.Assertions.notNull;

public class DocCodecProvider implements CodecProvider {

	private final BsonTypeClassMap bsonTypeClassMap;
	private final Transformer valueTransformer;

	public DocCodecProvider() {
		this(MRegistry.TYPE_CLASS_MAP);
	}

	public DocCodecProvider(final Transformer valueTransformer) {
		this(MRegistry.TYPE_CLASS_MAP, valueTransformer);
	}

	public DocCodecProvider(final BsonTypeClassMap bsonTypeClassMap) {
		this(bsonTypeClassMap, null);
	}

	public DocCodecProvider(final BsonTypeClassMap bsonTypeClassMap, final Transformer valueTransformer) {
		this.bsonTypeClassMap = notNull("bsonTypeClassMap", bsonTypeClassMap);
		this.valueTransformer = valueTransformer;
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> Codec<T> get(final Class<T> clazz, final CodecRegistry registry) {
		if (clazz == Doc.class) {
			return (Codec<T>)new DocCodec(registry, bsonTypeClassMap, valueTransformer);
		}

		return null;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		DocCodecProvider that = (DocCodecProvider)o;

		if (!bsonTypeClassMap.equals(that.bsonTypeClassMap)) {
			return false;
		}
		return Objects.equals(valueTransformer, that.valueTransformer);
	}

	@Override
	public int hashCode() {
		int result = bsonTypeClassMap.hashCode();
		result = 31 * result + (valueTransformer != null ? valueTransformer.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "DocCodecProvider{}";
	}

}
