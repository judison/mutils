package org.judison.mutils;

public class Projection extends BaseBson {

	public Projection() {}

	public Projection(String... fields) {
		add(fields);
	}

	/**
	 * Allows you to map a field or use expressions, works in aggregate pipeline only
	 */
	public Projection put(String name, Object value) {
		doc.put(name, value);
		return this;
	}

	public Projection add(String... fields) {
		for (String field: fields)
			if (field.charAt(0) == '-')
				doc.put(field.substring(1), -1);
			else if (field.charAt(0) == '+')
				doc.put(field.substring(1), +1);
			else
				doc.put(field, +1);
		return this;
	}

}