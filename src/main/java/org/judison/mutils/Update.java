package org.judison.mutils;

import java.util.ArrayList;
import java.util.List;

public class Update extends BaseBson {

	public Update() {}

	public Update put(String name, Object value) {
		doc.put(name, value);
		return this;
	}

	private void putIn(String operator, String field, Object value) {
		Doc sub = (Doc)doc.get(operator);
		if (sub == null) {
			sub = new Doc();
			doc.put(operator, sub);
		}
		sub.put(field, value);
	}

	private Object getIn(String operator, String field) {
		Doc sub = (Doc)doc.get(operator);
		if (sub == null)
			return null;
		return sub.get(field);
	}

	public Update set(String field, Object value) {
		putIn("$set", field, value);
		return this;
	}

	/**
	 * The $setOnInsert operator assigns values to fields during an upsert only when using the upsert option to the update() operation performs an insert.
	 */
	public Update setOnInsert(String field, Object value) {
		putIn("$setOnInsert", field, value);
		return this;
	}

	public Update unset(String... fields) {
		for (String field: fields)
			putIn("$unset", field, 1);
		return this;
	}

	public Update push(String array, Object... values) {
		for (Object value: values)
			_each("$push", array, value);
		return this;
	}

	public Update pull(String array, Object... values) {
		for (Object value: values)
			_each("$pull", array, value);
		return this;
	}

	public Update addToSet(String array, Object... values) {
		for (Object value: values)
			_each("$addToSet", array, value);
		return this;
	}

	private void _each(String operator, String array, Object value) {
		Object obj = getIn(operator, array);
		if (obj == null)
			putIn(operator, array, value);
		else if (obj instanceof Doc eh && eh.containsKey("$each")) {
			@SuppressWarnings("unchecked")
			List<Object> each = (List<Object>)eh.get("$each");
			each.add(value);
		} else {
			Doc eh = new Doc();
			List<Object> each = new ArrayList<>();
			each.add(obj);
			each.add(value);
			eh.put("$each", each);
			putIn(operator, array, eh);
		}
	}

	public Update pop(String field) {
		putIn("$pop", field, 1);
		return this;
	}

	public Update popFirst(String field) {
		putIn("$pop", field, -1);
		return this;
	}

	public Update rename(String oldName, String newName) {
		putIn("$rename", oldName, newName);
		return this;
	}

}