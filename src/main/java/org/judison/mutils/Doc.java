package org.judison.mutils;

import java.io.Serializable;
import java.io.StringWriter;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bson.BsonDocument;
import org.bson.BsonDocumentWrapper;
import org.bson.UuidRepresentation;
import org.bson.codecs.BsonValueCodecProvider;
import org.bson.codecs.Codec;
import org.bson.codecs.Decoder;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.Encoder;
import org.bson.codecs.EncoderContext;
import org.bson.codecs.IterableCodecProvider;
import org.bson.codecs.MapCodecProvider;
import org.bson.codecs.ValueCodecProvider;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.conversions.Bson;
import org.bson.json.JsonMode;
import org.bson.json.JsonReader;
import org.bson.json.JsonWriter;
import org.bson.json.JsonWriterSettings;
import org.bson.types.ObjectId;

import static java.lang.String.format;
import static org.bson.assertions.Assertions.isTrue;
import static org.bson.assertions.Assertions.notNull;

public class Doc implements Map<String, Object>, Serializable, Bson {

	private static final Codec<Doc> DEFAULT_CODEC =
		CodecRegistries.withUuidRepresentation(CodecRegistries.fromProviders(Arrays.asList(new ValueCodecProvider(), new IterableCodecProvider(),
				new BsonValueCodecProvider(), new DocCodecProvider(), new MapCodecProvider())), UuidRepresentation.STANDARD)
			.get(Doc.class);

	public static Doc parse(final String json) {
		return parse(json, DEFAULT_CODEC);
	}

	public static Doc parse(final String json, final Decoder<Doc> decoder) {
		notNull("codec", decoder);
		JsonReader bsonReader = new JsonReader(json);
		return decoder.decode(bsonReader, DecoderContext.builder().build());
	}

	private final LinkedHashMap<String, Object> map = new LinkedHashMap<>();

	public Doc() {}

	public Doc(String key, Object value) {
		put(key, value);
	}

	public Doc(Map<? extends String, ?> m) {
		putAll(m);
	}

	public Doc append(String key, Object value) {
		put(key, value);
		return this;
	}

	@Override
	public <C> BsonDocument toBsonDocument(final Class<C> documentClass, final CodecRegistry codecRegistry) {
		return new BsonDocumentWrapper<>(this, codecRegistry.get(Doc.class));
	}

	public <T> T get(final String key, final Class<T> clazz) {
		notNull("clazz", clazz);
		return clazz.cast(map.get(key));
	}

	@SuppressWarnings("unchecked")
	public <T> T get(final String key, final T defaultValue) {
		notNull("defaultValue", defaultValue);
		Object value = map.get(key);
		return value == null ? defaultValue : (T)value;
	}

	public <T> T getEmbedded(String dotKeys, final Class<T> clazz) {
		notNull("dotKeys", dotKeys);
		String[] keys = dotKeys.split("\\.");
		isTrue("dotKeys", keys.length > 0);
		notNull("clazz", clazz);
		return getEmbeddedValue(keys, clazz, null);
	}

	public <T> T getEmbedded(String dotKeys, final T defaultValue) {
		notNull("dotKeys", dotKeys);
		String[] keys = dotKeys.split("\\.");
		isTrue("dotKeys", keys.length > 0);
		notNull("defaultValue", defaultValue);
		return getEmbeddedValue(keys, null, defaultValue);
	}

	@SuppressWarnings("unchecked")
	private <T> T getEmbeddedValue(final String[] keys, final Class<T> clazz, final T defaultValue) {
		String lastKey = null;
		Object value = this;
		for (String key: keys) {
			if (value instanceof Doc doc)
				value = doc.get(key);
			else if (value == null)
				return defaultValue;
			else
				throw new ClassCastException(format("At key %s, the value is not a Document (%s)", lastKey, value.getClass().getName()));
			lastKey = key;
		}
		return clazz != null ? clazz.cast(value) : (T)value;
	}

	public Integer getInteger(final String key) {
		return (Integer)get(key);
	}

	/**
	 * Gets the value of the given key as a primitive int.
	 *
	 * @param key          the key
	 * @param defaultValue what to return if the value is null
	 * @return the value as an integer, which may be null
	 * @throws java.lang.ClassCastException if the value is not an integer
	 */
	public int getInteger(final String key, final int defaultValue) {
		return get(key, defaultValue);
	}

	/**
	 * Gets the value of the given key as a Long.
	 *
	 * @param key the key
	 * @return the value as a long, which may be null
	 * @throws java.lang.ClassCastException if the value is not an long
	 */
	public Long getLong(final String key) {
		return (Long)get(key);
	}

	/**
	 * Gets the value of the given key as a Double.
	 *
	 * @param key the key
	 * @return the value as a double, which may be null
	 * @throws java.lang.ClassCastException if the value is not an double
	 */
	public Double getDouble(final String key) {
		return (Double)get(key);
	}

	/**
	 * Gets the value of the given key as a String.
	 *
	 * @param key the key
	 * @return the value as a String, which may be null
	 * @throws java.lang.ClassCastException if the value is not a String
	 */
	public String getString(final String key) {
		return (String)get(key);
	}

	/**
	 * Gets the value of the given key as a Boolean.
	 *
	 * @param key the key
	 * @return the value as a Boolean, which may be null
	 * @throws java.lang.ClassCastException if the value is not an boolean
	 */
	public Boolean getBoolean(final String key) {
		return (Boolean)get(key);
	}

	/**
	 * Gets the value of the given key as a primitive boolean.
	 *
	 * @param key          the key
	 * @param defaultValue what to return if the value is null
	 * @return the value as a primitive boolean
	 * @throws java.lang.ClassCastException if the value is not a boolean
	 */
	public boolean getBoolean(final String key, final boolean defaultValue) {
		return get(key, defaultValue);
	}

	/**
	 * Gets the value of the given key as an ObjectId.
	 *
	 * @param key the key
	 * @return the value as an ObjectId, which may be null
	 * @throws java.lang.ClassCastException if the value is not an ObjectId
	 */
	public ObjectId getObjectId(final String key) {
		return (ObjectId)get(key);
	}

	public Instant getDate(final String key) {
		return (Instant)get(key);
	}

	public <T> List<T> getList(final String key, final Class<T> clazz) {
		notNull("clazz", clazz);
		return constructValuesList(key, clazz, null);
	}

	public <T> List<T> getList(final String key, final Class<T> clazz, final List<T> defaultValue) {
		notNull("defaultValue", defaultValue);
		notNull("clazz", clazz);
		return constructValuesList(key, clazz, defaultValue);
	}

	public Doc getDoc(final String key) {
		return get(key, Doc.class);
	}

	// Construct the list of values for the specified key, or return the default value if the value is null.
	// A ClassCastException will be thrown if an element in the list is not of type T.
	@SuppressWarnings("unchecked")
	private <T> List<T> constructValuesList(final String key, final Class<T> clazz, final List<T> defaultValue) {
		List<?> value = get(key, List.class);
		if (value == null) {
			return defaultValue;
		}

		for (Object item: value) {
			if (!clazz.isAssignableFrom(item.getClass())) {
				throw new ClassCastException(format("List element cannot be cast to %s", clazz.getName()));
			}
		}
		return (List<T>)value;
	}

	@SuppressWarnings("deprecation")
	public String toJson() {
		return toJson(JsonWriterSettings.builder().outputMode(JsonMode.RELAXED).build());
	}

	public String toJson(final JsonWriterSettings writerSettings) {
		return toJson(writerSettings, DEFAULT_CODEC);
	}

	@SuppressWarnings("deprecation")
	public String toJson(final Encoder<Doc> encoder) {
		return toJson(JsonWriterSettings.builder().outputMode(JsonMode.RELAXED).build(), encoder);
	}

	public String toJson(final JsonWriterSettings writerSettings, final Encoder<Doc> encoder) {
		JsonWriter writer = new JsonWriter(new StringWriter(), writerSettings);
		encoder.encode(writer, this, EncoderContext.builder().build());
		return writer.getWriter().toString();
	}

	// Vanilla Map methods delegate to map field

	@Override
	public int size() {
		return map.size();
	}

	@Override
	public boolean isEmpty() {
		return map.isEmpty();
	}

	@Override
	public boolean containsValue(final Object value) {
		return map.containsValue(value);
	}

	@Override
	public boolean containsKey(final Object key) {
		return map.containsKey(key);
	}

	@Override
	public Object get(final Object key) {
		return map.get(key);
	}

	@Override
	public Object put(final String key, final Object value) {
		return map.put(key, value);
	}

	@Override
	public Object remove(final Object key) {
		return map.remove(key);
	}

	@Override
	public void putAll(final Map<? extends String, ?> map) {
		this.map.putAll(map);
	}

	@Override
	public void clear() {
		map.clear();
	}

	@Override
	public Set<String> keySet() {
		return map.keySet();
	}

	@Override
	public Collection<Object> values() {
		return map.values();
	}

	@Override
	public Set<Map.Entry<String, Object>> entrySet() {
		return map.entrySet();
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Doc mo = (Doc)o;
		return map.equals(mo.map);
	}

	@Override
	public int hashCode() {
		return map.hashCode();
	}

	@Override
	public String toString() {
		return "Doc{"
			+ map
			+ '}';
	}

}
