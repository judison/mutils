package org.judison.mutils;

public class Group extends BaseBson {

	public Group(String _id) {
		doc.put("_id", _id);
	}

	private Group addField(String name, String accumulator, String expr) {
		doc.put(name, new Doc(accumulator, expr));
		return this;
	}

	private Group addField(String name, String accumulator, Doc value) {
		doc.put(name, new Doc(accumulator, value));
		return this;
	}

	public Group addToSet(String name, String expr) {
		return addField(name, "$addToSet", expr);
	}

	public Group avg(String name, String expr) {
		return addField(name, "$avg", expr);
	}

	public Group bottom(String name, String expr) {
		return addField(name, "$bottom", expr);
	}

	public Group count(String name) {
		return addField(name, "$count", new Doc());
	}

	public Group first(String name, String expr) {
		return addField(name, "$first", expr);
	}

	public Group last(String name, String expr) {
		return addField(name, "$last", expr);
	}

	public Group max(String name, String expr) {
		return addField(name, "$max", expr);
	}

	public Group mergeObjects(String name, String expr) {
		return addField(name, "$mergeObjects", expr);
	}

	public Group min(String name, String expr) {
		return addField(name, "$min", expr);
	}

	public Group push(String name, String expr) {
		return addField(name, "$push", expr);
	}

	public Group sum(String name, String expr) {
		return addField(name, "$sum", expr);
	}

	public Group top(String name, String expr) {
		return addField(name, "$top", expr);
	}

}
