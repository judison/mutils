package org.judison.mutils;

import java.util.function.Function;

//TODO falta mta coisa
// https://www.mongodb.com/docs/manual/meta/aggregation-quick-reference/#std-label-aggregation-expressions
@SuppressWarnings("unused")
public class Expr {

	public final Object value;

	public Expr(Object value) {
		this.value = value;
	}

	public static Expr field(String name) {
		if (!name.startsWith("$"))
			name = "$" + name;
		return new Expr(name);
	}

	public static Expr literal(Object value) {
		return new Expr(value);
	}

	@SuppressWarnings("unchecked")
	private static <D, S> D[] map(S[] src, Function<S, D> func) {
		D[] dst = (D[])new Object[src.length];
		for (int i = 0; i < src.length; i++)
			dst[i] = func.apply(src[i]);
		return dst;
	}

	//=========================================================================
	// Arithmetic Expression Operators
	//=========================================================================

	/** Returns the absolute value of a number. */
	public static Expr abs(Expr number) {
		return new Expr(new Doc("$abs", number.value));
	}

	/** Adds numbers to return the sum, or adds numbers and a date to return a new date. */
	public static Expr add(Expr... exprs) {
		return new Expr(new Doc("$add", map(exprs, (e) -> e.value)));
	}

	/** Returns the smallest integer greater than or equal to the specified number. */
	public static Expr ceil(Expr number) {
		return new Expr(new Doc("$ceil", number.value));
	}

	/** Returns the result of dividing the first number by the second. */
	public static Expr divide(Expr dividend, Expr divisor) {
		return new Expr(new Doc("$divide", new Object[]{dividend.value, divisor.value}));
	}

	/** Raises e to the specified exponent. */
	public static Expr exp(Expr exponent) {
		return new Expr(new Doc("$exp", exponent.value));
	}

	/** Returns the largest integer less than or equal to the specified number. */
	public static Expr floor(Expr number) {
		return new Expr(new Doc("$floor", number.value));
	}

	/** Calculates the natural log of a number. */
	public static Expr ln(Expr number) {
		return new Expr(new Doc("$ln", number.value));
	}

	/** Calculates the log of a number in the specified base. */
	public static Expr log(Expr number, Expr base) {
		return new Expr(new Doc("$log", new Object[]{number.value, base.value}));
	}

	/** Calculates the log base 10 of a number. */
	public static Expr log10(Expr number) {
		return new Expr(new Doc("$log10", number.value));
	}

	/** Returns the remainder of the first number divided by the second. */
	public static Expr mod(Expr dividend, Expr divisor) {
		return new Expr(new Doc("$mod", new Object[]{dividend.value, divisor.value}));
	}

	/** Multiplies numbers to return the product. */
	public static Expr multiply(Expr... exprs) {
		return new Expr(new Doc("$multiply", map(exprs, (e) -> e.value)));
	}

	/** Raises a number to the specified exponent. */
	public static Expr pow(Expr number, Expr exponent) {
		return new Expr(new Doc("$pow", new Object[]{number.value, exponent.value}));
	}

	/** Rounds a number to a whole integer. */
	public static Expr round(Expr number) {
		return round(number, literal(0));
	}

	/** Rounds a number to a whole integer or to a specified decimal place. */
	public static Expr round(Expr number, Expr place) {
		return new Expr(new Doc("$round", new Object[]{number.value, place.value}));
	}

	/** Calculates the square root of a positive number. */
	public static Expr sqrt(Expr number) {
		return new Expr(new Doc("$sqrt", number.value));
	}

	/** Subtracts two numbers to return the difference. */
	public static Expr subtract(Expr expr1, Expr expr2) {
		return new Expr(new Doc("$subtract", new Object[]{expr1.value, expr2.value}));
	}

	/** Truncates a number to a whole integer. */
	public static Expr trunc(Expr number) {
		return trunc(number, literal(0));
	}

	/** Truncates a number to a specified decimal place. */
	public static Expr trunc(Expr number, Expr place) {
		return new Expr(new Doc("$trunc", new Object[]{number.value, place.value}));
	}

	//=========================================================================
	// Array Expression Operators
	//=========================================================================

	//$arrayElemAt array idx // Returns the element at the specified array index.

	//$arrayToObject
	//
	//Converts an array of key value pairs to a document.

	//$concatArrays
	//
	//Concatenates arrays to return the concatenated array.

	//$filter
	//
	//Selects a subset of the array to return an array with only the elements that match the filter condition.

	//$first
	//
	//Returns the first array element. Distinct from $first accumulator.

	//$firstN
	//
	//Returns a specified number of elements from the beginning of an array. Distinct from the $firstN accumulator.

	//$in
	//
	//Returns a boolean indicating whether a specified value is in an array.

	//$indexOfArray
	//
	//Searches an array for an occurrence of a specified value and returns the array index of the first occurrence. If the substring is not found, returns -1.

	//$isArray
	//
	//Determines if the operand is an array. Returns a boolean.

	//$last
	//
	//Returns the last array element. Distinct from $last accumulator.

	//$lastN
	//
	//Returns a specified number of elements from the end of an array. Distinct from the $lastN accumulator.

	//$map
	//
	//Applies a subexpression to each element of an array and returns the array of resulting values in order. Accepts named parameters.

	//$maxN
	//
	//Returns the n largest values in an array. Distinct from the $maxN accumulator.

	//$minN
	//
	//Returns the n smallest values in an array. Distinct from the $minN accumulator.

	//$objectToArray
	//
	//Converts a document to an array of documents representing key-value pairs.

	//$range
	//
	//Outputs an array containing a sequence of integers according to user-defined inputs.

	//$reduce
	//
	//Applies an expression to each element in an array and combines them into a single value.

	//$reverseArray
	//
	//Returns an array with the elements in reverse order.

	//$size
	//
	//Returns the number of elements in the array. Accepts a single expression as argument.

	//$slice
	//
	//Returns a subset of an array.

	//$sortArray
	//
	//Sorts the elements of an array.

	//$zip
	//
	//Merge two arrays together.

	//=========================================================================
	// Boolean Expression Operators
	//=========================================================================

	//$and
	//
	//Returns true only when all its expressions evaluate to true. Accepts any number of argument expressions.

	//$not
	//
	//Returns the boolean value that is the opposite of its argument expression. Accepts a single argument expression.

	//$or
	//
	//Returns true when any of its expressions evaluates to true. Accepts any number of argument expressions.
}

