package org.judison.mutils;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

import org.bson.BsonType;
import org.bson.Transformer;
import org.bson.codecs.BsonCodecProvider;
import org.bson.codecs.BsonTypeClassMap;
import org.bson.codecs.BsonValueCodecProvider;
import org.bson.codecs.DocumentCodecProvider;
import org.bson.codecs.EnumCodecProvider;
import org.bson.codecs.IterableCodecProvider;
import org.bson.codecs.JsonObjectCodecProvider;
import org.bson.codecs.MapCodecProvider;
import org.bson.codecs.ValueCodecProvider;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.jsr310.Jsr310CodecProvider;
import org.bson.codecs.pojo.PojoCodecProvider;

import com.mongodb.DBObjectCodecProvider;
import com.mongodb.DBRefCodecProvider;
import com.mongodb.DocumentToDBRefTransformer;
import com.mongodb.Jep395RecordCodecProvider;
import com.mongodb.client.gridfs.codecs.GridFSFileCodecProvider;
import com.mongodb.client.model.geojson.codecs.GeoJsonCodecProvider;

import static java.util.Arrays.asList;

public class MRegistry {

	public static final BsonTypeClassMap TYPE_CLASS_MAP;
	public static final CodecRegistry CODEC_REGISTRY;

	private static final Transformer TRANSFORMER;

	static {
		Map<BsonType, Class<?>> map = new HashMap<>();
		map.put(BsonType.DOCUMENT, Doc.class);
		map.put(BsonType.DATE_TIME, Instant.class);
		TYPE_CLASS_MAP = new BsonTypeClassMap(map);

		TRANSFORMER = src -> {
//				if (src != null)
//					System.out.println("trasf " + src.getClass().getName());
			if (src instanceof Map<?, ?> m) {
				//noinspection unchecked
				return new Doc((Map<String, Object>)m);
			}
			return src;
		};

		CODEC_REGISTRY = CodecRegistries.fromProviders(asList(new ValueCodecProvider(),
			new BsonValueCodecProvider(),
			new DBRefCodecProvider(),
			new DBObjectCodecProvider(),
			new DocCodecProvider(),
			new DocumentCodecProvider(new DocumentToDBRefTransformer()),
			new IterableCodecProvider(TRANSFORMER),
			new MapCodecProvider(TYPE_CLASS_MAP, TRANSFORMER),//, new DocumentToDBRefTransformer()),
			new GeoJsonCodecProvider(),
			new GridFSFileCodecProvider(),
			new Jsr310CodecProvider(),
			new JsonObjectCodecProvider(),
			new BsonCodecProvider(),
			new EnumCodecProvider(),
			new Jep395RecordCodecProvider(),
			PojoCodecProvider.builder().automatic(true).build()
		));
	}

}
