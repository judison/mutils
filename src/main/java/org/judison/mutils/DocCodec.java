package org.judison.mutils;

import java.util.Map;
import java.util.UUID;

import org.bson.BsonDocument;
import org.bson.BsonDocumentWriter;
import org.bson.BsonReader;
import org.bson.BsonType;
import org.bson.BsonValue;
import org.bson.BsonWriter;
import org.bson.Transformer;
import org.bson.codecs.BsonTypeClassMap;
import org.bson.codecs.BsonTypeCodecMap;
import org.bson.codecs.Codec;
import org.bson.codecs.CollectibleCodec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;
import org.bson.codecs.IdGenerator;
import org.bson.codecs.ObjectIdGenerator;
import org.bson.codecs.configuration.CodecRegistry;

import static org.bson.assertions.Assertions.notNull;

public class DocCodec implements CollectibleCodec<Doc> {

	private static final String ID_FIELD_NAME = "_id";
	private static final CodecRegistry DEFAULT_REGISTRY = MRegistry.CODEC_REGISTRY;
	private static final BsonTypeCodecMap DEFAULT_BSON_TYPE_CODEC_MAP = new BsonTypeCodecMap(MRegistry.TYPE_CLASS_MAP, DEFAULT_REGISTRY);
	private static final IdGenerator DEFAULT_ID_GENERATOR = new ObjectIdGenerator();

	private final BsonTypeCodecMap bsonTypeCodecMap;
	private final CodecRegistry registry;
	private final IdGenerator idGenerator;
	private final Transformer valueTransformer;

	public DocCodec() {
		this(DEFAULT_REGISTRY, DEFAULT_BSON_TYPE_CODEC_MAP, null);
	}

	public DocCodec(final CodecRegistry registry) {
		this(registry, MRegistry.TYPE_CLASS_MAP);
	}

	public DocCodec(final CodecRegistry registry, final BsonTypeClassMap bsonTypeClassMap) {
		this(registry, bsonTypeClassMap, null);
	}

	public DocCodec(final CodecRegistry registry, final BsonTypeClassMap bsonTypeClassMap, final Transformer valueTransformer) {
		this(registry, new BsonTypeCodecMap(notNull("bsonTypeClassMap", bsonTypeClassMap), registry), valueTransformer);
	}

	private DocCodec(final CodecRegistry registry, final BsonTypeCodecMap bsonTypeCodecMap, final Transformer valueTransformer) {
		this(registry, bsonTypeCodecMap, DEFAULT_ID_GENERATOR, valueTransformer);
	}

	private DocCodec(final CodecRegistry registry, final BsonTypeCodecMap bsonTypeCodecMap, final IdGenerator idGenerator,
					 final Transformer valueTransformer) {
		this.registry = notNull("registry", registry);
		this.bsonTypeCodecMap = bsonTypeCodecMap;
		this.idGenerator = idGenerator;
		this.valueTransformer = valueTransformer != null ? valueTransformer : new Transformer() {
			@Override
			public Object transform(final Object value) {
				return value;
			}
		};
	}

	@Override
	public boolean documentHasId(final Doc document) {
		return document.containsKey(ID_FIELD_NAME);
	}

	@Override
	public BsonValue getDocumentId(final Doc document) {
		if (!documentHasId(document)) {
			throw new IllegalStateException("The document does not contain an _id");
		}

		Object id = document.get(ID_FIELD_NAME);
		if (id instanceof BsonValue) {
			return (BsonValue)id;
		}

		BsonDocument idHoldingDocument = new BsonDocument();
		BsonWriter writer = new BsonDocumentWriter(idHoldingDocument);
		writer.writeStartDocument();
		writer.writeName(ID_FIELD_NAME);
		writeValue(writer, EncoderContext.builder().build(), id);
		writer.writeEndDocument();
		return idHoldingDocument.get(ID_FIELD_NAME);
	}

	@Override
	public Doc generateIdIfAbsentFromDocument(final Doc document) {
		if (!documentHasId(document)) {
			document.put(ID_FIELD_NAME, idGenerator.generate());
		}
		return document;
	}

	@Override
	public void encode(final BsonWriter writer, final Doc document, final EncoderContext encoderContext) {
		writer.writeStartDocument();

		beforeFields(writer, encoderContext, document);

		for (final Map.Entry<String, Object> entry: ((Map<String, Object>)document).entrySet()) {
			if (skipField(encoderContext, entry.getKey())) {
				continue;
			}
			writer.writeName(entry.getKey());
			writeValue(writer, encoderContext, entry.getValue());
		}
		writer.writeEndDocument();
	}

	@Override
	public Doc decode(final BsonReader reader, final DecoderContext decoderContext) {
		Doc document = new Doc();

		reader.readStartDocument();
		while (reader.readBsonType() != BsonType.END_OF_DOCUMENT) {
			String fieldName = reader.readName();
			document.put(fieldName, readValue(reader, decoderContext, bsonTypeCodecMap, registry, valueTransformer));
		}

		reader.readEndDocument();

		return document;
	}

	static Object readValue(final BsonReader reader, final DecoderContext decoderContext,
							final BsonTypeCodecMap bsonTypeCodecMap,
							final CodecRegistry registry, final Transformer valueTransformer) {

		BsonType bsonType = reader.getCurrentBsonType();
		if (bsonType == BsonType.NULL) {
			reader.readNull();
			return null;
		} else {
			Codec<?> codec = bsonTypeCodecMap.get(bsonType);

			if (bsonType == BsonType.BINARY && reader.peekBinarySize() == 16)
				if (reader.peekBinarySubType() == 4)
					codec = registry.get(UUID.class);

			return valueTransformer.transform(codec.decode(reader, decoderContext));
		}
	}

	@Override
	public Class<Doc> getEncoderClass() {
		return Doc.class;
	}

	private void beforeFields(final BsonWriter bsonWriter, final EncoderContext encoderContext, final Map<String, Object> document) {
		if (encoderContext.isEncodingCollectibleDocument() && document.containsKey(ID_FIELD_NAME)) {
			bsonWriter.writeName(ID_FIELD_NAME);
			writeValue(bsonWriter, encoderContext, document.get(ID_FIELD_NAME));
		}
	}

	private boolean skipField(final EncoderContext encoderContext, final String key) {
		return encoderContext.isEncodingCollectibleDocument() && key.equals(ID_FIELD_NAME);
	}

	@SuppressWarnings({"unchecked", "rawtypes"})
	private void writeValue(final BsonWriter writer, final EncoderContext encoderContext, final Object value) {
		if (value == null) {
			writer.writeNull();
		} else {
			Codec codec = registry.get(value.getClass());
			encoderContext.encodeWithChildContext(codec, writer, value);
		}
	}

}
