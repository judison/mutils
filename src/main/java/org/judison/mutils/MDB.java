package org.judison.mutils;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoDatabase;

public abstract class MDB {

	private final MongoClient client;
	private final String name;
	private MongoDatabase db;
	private final Map<String, MCollection<?, ?>> collections = new HashMap<>();

	public MDB(MongoClient client, String name) {
		this.client = client;
		this.name = name;
	}

	public <K, V> MCollection<K, V> getCollection(String name, Class<K> keyClass, Class<V> valueClass, Function<V, K> getId) {
		String key = name + ":" + keyClass.getName() + ":" + valueClass.getName();
		synchronized (collections) {
			@SuppressWarnings("unchecked")
			MCollection<K, V> coll = (MCollection<K, V>)collections.get(key);
			if (coll == null)
				collections.put(key, coll = new MCollection<>(this, name, keyClass, valueClass, getId));
			return coll;
		}
	}

	public <C extends MCollection<K, V>, K, V> C getCollection(String name, Class<K> keyClass, Class<V> valueClass, Supplier<C> supplier) {
		String key = name + ":" + keyClass.getName() + ":" + valueClass.getName();
		synchronized (collections) {
			@SuppressWarnings("unchecked")
			C coll = (C)collections.get(key);
			if (coll == null)
				collections.put(key, coll = supplier.get());
			return coll;
		}
	}

	public MongoDatabase getMongoDatabase() {
		if (db == null)
			this.db = client.getDatabase(name).withCodecRegistry(MRegistry.CODEC_REGISTRY);

		return db;
	}

	public void close() {
		if (db != null) {
			db = null;
		}
	}

	public AggregateItr<Doc> aggregate(Pipeline pipeline) {
		return aggregate(pipeline, Doc.class);
	}

	public <T> AggregateItr<T> aggregate(Pipeline pipeline, Class<T> cls) {
		return new AggregateItr<T>(getMongoDatabase().aggregate(pipeline.toDocList(), cls));
	}

}
